#usage python3 [filepath]/main.py --playlistsPath [.../.config/cmus/playlists]

import os;
from shutil import copyfile
import urllib.parse;
import argparse

# 1. Parse arguments
parser = argparse.ArgumentParser(description='CMUS Playlist Exporter')
parser.add_argument('--playlistsPath', dest='playlistsPath', help='The cmus playlists path (usually located in .config/cmus/playlists).', default='', action='store');
args = parser.parse_args();

def CheckIfItemIsDuplicate(index,pl_item,pl_array):
    result = False;
    for i in range(0,len(pl_array)):
        if pl_item == pl_array[i] and index > i:
            result = True;
    return result;

def ExportPlaylist(playlistname,playlistURL):
    print('Exporting playlist ('+playlistname+')');
    m3uString = '#EXTM3U\n';
    file = open(playlistURL,'r');
    playlist_array = [];
    for line in file:
        playlist_array.append(line.replace('\n',''));

    for i in range(0,len(playlist_array)):
        is_duplicate = CheckIfItemIsDuplicate(i,playlist_array[i],playlist_array);
        filepatharray = playlist_array[i].split('/');
        filename = filepatharray[len(filepatharray)-1];
        if is_duplicate == False:
            dur_seconds = '';
            artist = '';
            song = '';
            print('Exporting item ('+filename+')');
            m3uString = m3uString+'#EXTINF:'+dur_seconds+','+artist+' - '+song+'\n'+urllib.parse.quote(filename)+'\n';
            copyfile(playlist_array[i],localDirectory+'/exported/'+filename);
        else:
            print('Skipping because it is duplicate ('+filename+')');

    with open(localDirectory+'/exported/'+playlistname+'.m3u','w') as pl_file:
        pl_file.write(m3uString);

print('Running CMUS Playlist Exporter...');

if args.playlistsPath == '':
    playlistsPath = input('PlaylistPath not set. Please drag the local cmus playlist directory to your terminal and press enter.').replace(' ','');
    print('Thank you!');
else:
    playlistsPath = args.playlistsPath;

print('PlaylistsPath: ',playlistsPath);

localDirectory = os.path.dirname(os.path.abspath(__file__));

print('Loading playlists...');
playlists = [n for n in os.listdir(playlistsPath) if not n.startswith(".")]

include_all = input('Du you want to export all ('+str(len(playlists))+') playlists? (y/n) ');

for i in range(0,len(playlists)):
    if include_all == 'y':
        print('Exporting '+str(playlists[i])+'...');
        ExportPlaylist(playlists[i],playlistsPath+'/'+playlists[i]);
    else:
        include_this = input('Do you want to export the playlist: '+str(playlists[i])+'? (y/n) ');
        if include_this == 'y':
            ExportPlaylist(playlists[i],playlistsPath+'/'+playlists[i]);
        else:
            print('Skipping list...');

print('END OF LINE.');
